from keras.optimizers import Nadam, Adam, RMSprop

STEP_SIZE = 4
N_USUARIOS = 970

FILLED_DATA = True

LOCATION_INPUT_DIM = 3
TIME_INPUT_DIM = 24

RNNs = {0: "GRU_baseline_original", 1: "GRU_baseline_filled", 2: "STF_RNN_original", 3: "STF_RNN_filled",
        4: "LSTM_48hours_baseline_original", 5: "LSTM_48hours_baseline_filled", 6: "Deep_move", 7: "MAP_original", 8: "MAP_filled"}

FIGURES_DIR = {0: "outputs/next_location_predict_baseline/figures/gru_baseline_original/",
               1: "outputs/next_location_predict_baseline/figures/gru_baseline_filled/",
               2: "outputs/next_location_predict_baseline/figures/stf_rnn_original/",
               3: "outputs/next_location_predict_baseline/figures/stf_rnn_filled/",
               4: "outputs/next_location_predict_baseline/figures/lstm_48hours_baseline_original/",
               5: "outputs/next_location_predict_baseline/figures/lstm_48hours_baseline_filled/",
               6: "outputs/next_location_predict_baseline/figures/deep_move/",
               7: "outputs/next_location_predict_baseline/figures/map_original/",
               8: "outputs/next_location_predict_baseline/figures/map_filled/"}

N_TESTES = 5
EPOCHS = 10

N_SPLITS = 5

DEEP_MOVE = {0: False, 1: False, 2: False, 3: False, 4: False, 5: False, 6: True, 7: False, 8: False}

CLASS_WEIGHT = {0: {0: 2, 1: 2, 2: 1},
                1: {0: 1, 1: 8, 2: 1},
                2: {0: 2, 1: 2, 2: 1},
                3: {0: 1, 1: 2, 2: 1},
                4: {0: 1.8, 1: 1.5, 2: 1},
                5: {0: 1, 1: 1.4, 2: 1},
                6: {0: 1, 1: 1, 2: 1},
                7: {0: 1.8, 1: 1.7, 2: 1},
                8: {0: 1, 1: 2, 2: 1}}

INDEX_SIZE = 0

SIZE = {0: "_1mil_", 1: "_10mil_"}[INDEX_SIZE]

DATASET = {0: {"file_name": "location_sequence_24hours" + SIZE + "usuarios.csv", "time_num_classes": 24},
           1: {"file_name": "location_sequence_24hours_filled" + SIZE + "usuarios.csv", "time_num_classes": 24},
           2: {"file_name": "location_sequence_24hours" + SIZE + "usuarios.csv", "time_num_classes": 24},
           3: {"file_name": "location_sequence_24hours_filled" + SIZE + "usuarios.csv", "time_num_classes": 24},
           4: {"file_name": "location_sequence_48hours" + SIZE + "usuarios.csv", "time_num_classes": 48},
           5: {"file_name": "location_sequence_48hours_filled" + SIZE + "usuarios.csv", "time_num_classes": 48},
           6: {"file_name": "location_sequence_24hours" + SIZE + "usuarios.csv", "time_num_classes": 24},
           7: {"file_name": "location_sequence_24hours" + SIZE + "usuarios.csv", "time_num_classes": 24},
           8: {"file_name": "location_sequence_24hours_filled" + SIZE + "usuarios.csv", "time_num_classes": 24}}



TIME_OUTPUT = {0: False, 1: False, 2: False, 3: False, 4: False, 5: False, 6: False, 7: False, 8: False}

N_LOCATION_OUTPUT = {0: 2, 1: 2, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1, 7: 1, 8: 1}

nadam = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, amsgrad=False)
rm = RMSprop(lr=0.0005)
OPTIMIZER = {0: "adam", 1: "adam", 2: "adam", 3: "adam", 4: "adam", 5: "adam", 6: "adam", 7: "adam", 8: "adam"}

VALIDATION_METRICS_NAMES = {0: [("loss", "val_loss" , "Loss"),
                 ("ma_activation_1_loss", "val_ma_activation_1_loss", "Loss multi-head attention"),
                 ("pe_activation_1_loss", "val_pe_activation_1_loss", "Loss positional encoding"),
                 ("ma_activation_1_acc", "val_ma_activation_1_acc", "Accuracy multi-head attention"),
                 ("pe_activation_1_acc", "val_pe_activation_1_acc", "Accuracy positional encoding")
                 ],
                            1: [("loss", "val_loss" , "Loss"),
                 ("ma_activation_1_loss", "val_ma_activation_1_loss", "Loss multi-head attention"),
                 ("pe_activation_1_loss", "val_pe_activation_1_loss", "Loss positional encoding"),
                 ("ma_activation_1_acc", "val_ma_activation_1_acc", "Accuracy multi-head attention"),
                 ("pe_activation_1_acc", "val_pe_activation_1_acc", "Accuracy positional encoding")
                 ],
                            2: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")
                 ],
                            3: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")
                 ],
                            4: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")
                 ],
                            5: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")
                 ],
                            6: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")
                 ],
                            7: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")
                 ],
                            8: [("loss", "val_loss", "Loss location"),
                                ("acc", "val_acc", "Accuracy location")
                 ]}
