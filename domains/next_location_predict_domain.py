import copy
import datetime as dt
import ast
import numpy as np
import pandas as pd
from sklearn.model_selection import KFold, StratifiedKFold

from models.next_location_predict_model.gru_enhanced import GRUenhaced
from models.next_location_predict_model.gru_enhanced_entropy_all_days import GRUenhaced_EntropyAllDays
from models.next_location_predict_model.gru_enhanced_grouped_users_routine import GRUenhacedGroupedUsersRoutine
from models.next_location_predict_model.gru_enhanced_1location_output import GRUenhaced_1location_output
from models.next_location_predict_model.gru_enhanced_1location_filled import GRUenhaced_1location_filled
from models.next_location_predict_model.gru_enhanced_1location_original import GRUenhaced_1location_original
from keras.utils import np_utils
from keras.losses import categorical_crossentropy
from sklearn.preprocessing import OneHotEncoder
import sklearn.metrics as skm
from utils.next_location_predict_util import sequence_to_x_y, remove_hour_from_sequence_x, \
    remove_hour_from_sequence_y, sequence_tuples_to_ndarray_x, sequence_tuples_to_ndarray_y, one_hot_decoding, \
    hours_shift_x, sequence_tuples_to_spatial_and_temporal_ndarrays, return_hour_from_sequence_y, \
    sequence_tuples_to_spatial_temporal_and_feature3_ndarrays

class NextLocationPredictDomain:

    def model(self, model_name):

        try:
            if model_name == "GRUenhaced":
                return GRUenhaced().build()
            elif model_name == "GRUenhaced_EntropyAllDays":
                return GRUenhaced_EntropyAllDays().build()
            elif model_name == "GRUenhaced_GroupedUsersRoutine":
                return GRUenhacedGroupedUsersRoutine().build()
            elif model_name == "GRU_enhaced_1location_original":
                return GRUenhaced_1location_original().build()
            elif model_name == "GRU_enhaced_1location_filled":
                return GRUenhaced_1location_filled().build()
            else:
                raise ValueError("Selecione um modelo")
        except Exception as e:
            raise e

    def extract_train_test_dataset(self,
                                   solution_id,
                                   filename,
                                   location_num_classes,
                                   time_num_classes,
                                   use_entropy,
                                   entropy_num_classes,
                                   timeOutput,
                                   n_location_outputs,
                                   features,
                                   step_size):

        if features == 2:
            return self.two_features(solution_id,
                                   filename,
                                   location_num_classes,
                                   time_num_classes,
                                   use_entropy,
                                   entropy_num_classes,
                                   timeOutput,
                                   n_location_outputs)

        elif features == 3:
            return self.three_features(solution_id,
                                     filename,
                                     location_num_classes,
                                     time_num_classes,
                                     use_entropy,
                                     entropy_num_classes,
                                     timeOutput,
                                     n_location_outputs,
                                       step_size)


    def two_features(self,
                     solution_id,
                     filename,
                     location_num_classes,
                     time_num_classes,
                     use_entropy,
                     entropy_num_classes,
                     timeOutput,
                     n_location_outputs):
        df = pd.read_csv(filename)

        # Convert string of list to list
        users_list = []
        entropies = []
        self.max_user_id = df['user_id'].max() + 1
        for i in range(df['location_sequence'].shape[0]):
            user_list = []
            user_id = df['user_id'].iloc[i]
            user_sequence = df['location_sequence'].iloc[i]
            user_sequence = user_sequence.replace("[", "").replace("]", "").split("), ")
            for e in user_sequence:
                e = e.replace("(", "").replace(")", "")
                if use_entropy:
                    location, hour, entropy = e.split(",")
                    entropy = float(entropy)
                    entropies.append(entropy)
                else:
                    location, hour = e.split(",")
                location = int(location)
                hour = int(hour)
                id = user_id
                if use_entropy:
                    user_list.append((location, hour, id, entropy))
                else:
                    user_list.append((location, hour, id))
            users_list.append(user_list)

        # print("ola: ", entropies)
        if use_entropy:
            users_list = self.entropy_to_integer(users_list, entropies, entropy_num_classes)

        # Get train and test datasets from user
        # users_list = users_list[:N_USUARIOS]

        X_train_concat = []
        X_test_concat = []
        y_train_concat = []
        y_test_concat = []
        for user_sequence in users_list:
            size = len(user_sequence)
            train_size = int(size * 0.7)
            test_size = size - train_size
            train = user_sequence[:train_size]
            test = user_sequence[train_size::]
            X_train, y_train = sequence_to_x_y(train)
            X_test, y_test = sequence_to_x_y(test)

            X_train_concat = X_train_concat + X_train
            X_test_concat = X_test_concat + X_test
            y_train_concat = y_train_concat + y_train
            y_test_concat = y_test_concat + y_test

        X_train = X_train_concat
        X_test = X_test_concat
        y_train = y_train_concat
        y_test = y_test_concat

        y_train_hours = return_hour_from_sequence_y(y_train)
        y_test_hours = return_hour_from_sequence_y(y_test)

        # Remove hours. Currently training without events hour
        # X_train = remove_hour_from_sequence_x(X_train)
        # X_test = remove_hour_from_sequence_x(X_test)
        y_train = remove_hour_from_sequence_y(y_train)
        y_test = remove_hour_from_sequence_y(y_test)

        # Sequence tuples to ndarray. Use it if the remove hour function was not called
        # X_train = sequence_tuples_to_ndarray_x(X_train)
        # X_test = sequence_tuples_to_ndarray_x(X_test)
        # y_train = sequence_tuples_to_ndarray_y(y_train)
        # y_test = sequence_tuples_to_ndarray_y(y_test)

        # Sequence tuples to [spatial[,step_size], temporal[,step_size]] ndarray. Use with embedding layer.
        if use_entropy:
            X_train = sequence_tuples_to_spatial_temporal_and_feature3_ndarrays(X_train, id=True)
            X_test = sequence_tuples_to_spatial_temporal_and_feature3_ndarrays(X_test, id=True)
        else:
            X_train = sequence_tuples_to_spatial_and_temporal_ndarrays(X_train, id=True)
            X_test = sequence_tuples_to_spatial_and_temporal_ndarrays(X_test, id=True)

        # X_train = np.asarray(X_train)
        # X_train = X_train.reshape(X_train.shape + (1,)) # perform it if the step doesn't have hour
        y_train = np.asarray(y_train)
        # X_test = np.asarray(X_test)
        # X_test = X_test.reshape(X_test.shape + (1,))
        y_test = np.asarray(y_test)

        # Convert integers to one-hot-encoding. It is important to convert the y (labels) to that.

        # X_train = np_utils.to_categorical(X_train)
        # X_test = np_utils.to_categorical(X_test)

        y_train_2 = np_utils.to_categorical(y_train, num_classes=location_num_classes)
        y_train = np_utils.to_categorical(y_train, num_classes=location_num_classes)
        y_train_hours = np_utils.to_categorical(y_train_hours, num_classes=time_num_classes)
        y_test_2 = np_utils.to_categorical(y_test, num_classes=location_num_classes)
        y_test = np_utils.to_categorical(y_test, num_classes=location_num_classes)
        y_test_hours = np_utils.to_categorical(y_test_hours, num_classes=time_num_classes)

        # when using multiple output [location, hour]
        if timeOutput:
            y_train = [y_train, y_train_hours]
            y_test = [y_test, y_test_hours]
        else:
            if n_location_outputs == 2:
                y_train = [y_train, y_train_2]
                y_test = [y_test, y_test_2]
            elif n_location_outputs == 1:
                y_train = [y_train]
                y_test = [y_test]

        return X_train, X_test, y_train, y_test

    def three_features(self,
                     solution_id,
                     filename,
                     location_num_classes,
                     time_num_classes,
                     use_entropy,
                     entropy_num_classes,
                     timeOutput,
                     n_location_outputs,
                        step_size):
        df = pd.read_csv(filename)

        # Convert string of list to list
        users_list = []
        entropies = df['user_entropy'].tolist()
        day_type_n_events = {0: {0: 0, 1: 0, 2: 0}, 1: {0: 0, 1: 0, 2: 0}}
        self.max_user_id = df['user_id'].max() + 1
        for i in range(df['location_sequence'].shape[0]):
            user_list = []
            user_id = df['user_id'].iloc[i]
            user_sequence = df['location_sequence'].iloc[i]
            user_sequence = user_sequence.replace("[", "").replace("]", "").split("), ")
            for e in user_sequence:
                e = e.replace("(", "").replace(")", "")
                #location, hour, feature_3, datetime = e.split(",")
                datetime, hour, location = e.split(",")
                datetime = str(datetime).replace("'", "")
                location = int(location)
                hour = int(hour)
                id = int(user_id)
                feature_3 = self.day_type(dt.datetime.strptime(datetime, "%Y-%m-%d %H:%M:%S"))
                #datetime = dt.datetime.strptime(datetime, "%Y-%m-%d %H:%M:%S")
                user_list.append((location, hour, id, feature_3))
                day_type_n_events[feature_3][location] = day_type_n_events[feature_3][location] + 1
            users_list.append(user_list)

        # print("ola: ", entropies)
        if use_entropy:
            users_list = self.entropy_to_integer(users_list, entropies, entropy_num_classes)

        # Get train and test datasets from user
        # users_list = users_list[:N_USUARIOS]

        X_train_concat = []
        X_test_concat = []
        y_train_concat = []
        y_test_concat = []
        for user_sequence in users_list:
            size = len(user_sequence)
            train_size = int(size * 0.7)
            test_size = size - train_size
            train = user_sequence[:train_size]
            test = user_sequence[train_size::]
            X_train, y_train = sequence_to_x_y(train, step_size)
            X_test, y_test = sequence_to_x_y(test, step_size)

            X_train_concat = X_train_concat + X_train
            X_test_concat = X_test_concat + X_test
            y_train_concat = y_train_concat + y_train
            y_test_concat = y_test_concat + y_test

        X_train = X_train_concat
        X_test = X_test_concat
        y_train = y_train_concat
        y_test = y_test_concat

        y_train_hours = return_hour_from_sequence_y(y_train)
        y_test_hours = return_hour_from_sequence_y(y_test)

        # Remove hours. Currently training without events hour
        # X_train = remove_hour_from_sequence_x(X_train)
        # X_test = remove_hour_from_sequence_x(X_test)
        y_train = remove_hour_from_sequence_y(y_train)
        y_test = remove_hour_from_sequence_y(y_test)

        # Sequence tuples to ndarray. Use it if the remove hour function was not called
        # X_train = sequence_tuples_to_ndarray_x(X_train)
        # X_test = sequence_tuples_to_ndarray_x(X_test)
        # y_train = sequence_tuples_to_ndarray_y(y_train)
        # y_test = sequence_tuples_to_ndarray_y(y_test)

        # Sequence tuples to [spatial[,step_size], temporal[,step_size]] ndarray. Use with embedding layer.
        X_train = sequence_tuples_to_spatial_temporal_and_feature3_ndarrays(X_train, id=True)
        X_test = sequence_tuples_to_spatial_temporal_and_feature3_ndarrays(X_test, id=True)

        # X_train = np.asarray(X_train)
        # X_train = X_train.reshape(X_train.shape + (1,)) # perform it if the step doesn't have hour
        y_train = np.asarray(y_train)
        # X_test = np.asarray(X_test)
        # X_test = X_test.reshape(X_test.shape + (1,))
        y_test = np.asarray(y_test)

        # Convert integers to one-hot-encoding. It is important to convert the y (labels) to that.

        # X_train = np_utils.to_categorical(X_train)
        # X_test = np_utils.to_categorical(X_test)

        y_train_2 = np_utils.to_categorical(y_train, num_classes=location_num_classes)
        y_train = np_utils.to_categorical(y_train, num_classes=location_num_classes)
        y_train_hours = np_utils.to_categorical(y_train_hours, num_classes=time_num_classes)
        y_test_2 = np_utils.to_categorical(y_test, num_classes=location_num_classes)
        y_test = np_utils.to_categorical(y_test, num_classes=location_num_classes)
        y_test_hours = np_utils.to_categorical(y_test_hours, num_classes=time_num_classes)

        # when using multiple output [location, hour]
        if timeOutput:
            y_train = [y_train, y_train_hours]
            y_test = [y_test, y_test_hours]
        else:
            if n_location_outputs == 2:
                y_train = [y_train, y_train_2]
                y_test = [y_test, y_test_2]
            elif n_location_outputs == 1:
                y_train = [y_train]
                y_test = [y_test]

        print("Quantidade de eventos em cada local por tipo do dia")
        print("Dia de semana: ")
        print("Casa", day_type_n_events[0][0])
        print("Outro", day_type_n_events[0][1])
        print("Deslocamento", day_type_n_events[0][2])

        print("Final de semana: ")
        print("Casa", day_type_n_events[1][0])
        print("Outro", day_type_n_events[1][1])
        print("Deslocamento", day_type_n_events[1][2])

        return X_train, X_test, y_train, y_test

    def extract_train_test_indexes_k_fold(self,
                     filename,
                     use_entropy,
                     entropy_num_classes,
                    n_splits):
        print("filename: ", filename)
        df = pd.read_csv(filename)

        # Convert string of list to list
        users_list = []
        entropies = df['user_entropy'].tolist()
        day_type_n_events = {0: {0: 0, 1: 0, 2: 0}, 1: {0: 0, 1: 0, 2: 0}}
        self.max_user_id = df['user_id'].max() + 1
        for i in range(df['location_sequence'].shape[0]):
            user_list = []
            user_id = df['user_id'].iloc[i]
            user_sequence = df['location_sequence'].iloc[i]
            user_sequence = user_sequence.replace("[", "").replace("]", "").split("), ")
            for e in user_sequence:
                e = e.replace("(", "").replace(")", "")
                #location, hour, feature_3, datetime = e.split(",")
                datetime, hour, location = e.split(",")
                datetime = str(datetime).replace("'", "")
                location = int(location)
                hour = int(hour)
                id = int(user_id)
                feature_3 = self.day_type(dt.datetime.strptime(datetime, "%Y-%m-%d %H:%M:%S"))
                #datetime = dt.datetime.strptime(datetime, "%Y-%m-%d %H:%M:%S")
                user_list.append((location, hour, id, feature_3))
                day_type_n_events[feature_3][location] = day_type_n_events[feature_3][location] + 1
            users_list.append(user_list)

        # print("ola: ", entropies)
        if use_entropy:
            users_list = self.entropy_to_integer(users_list, entropies, entropy_num_classes)

        # Get train and test datasets from user
        # users_list = users_list[:N_USUARIOS]

        kf = KFold(n_splits=n_splits)
        users_train_indexes = [None] * n_splits
        users_test_indexes = [None] * n_splits
        for user in users_list:
            i = 0
            for train_indexes, test_indexes in kf.split(user):
                if users_train_indexes[i] is None:
                    users_train_indexes[i] = [train_indexes]
                    users_test_indexes[i] = [test_indexes]
                else:
                    users_train_indexes[i].append(train_indexes)
                    users_test_indexes[i].append(test_indexes)
                i = i + 1


        print("Quantidade de eventos em cada local por tipo do dia")
        print("Dia de semana: ")
        print("Casa", day_type_n_events[0][0])
        print("Outro", day_type_n_events[0][1])
        print("Deslocamento", day_type_n_events[0][2])

        print("Final de semana: ")
        print("Casa", day_type_n_events[1][0])
        print("Outro", day_type_n_events[1][1])
        print("Deslocamento", day_type_n_events[1][2])

        return users_list, users_train_indexes, users_test_indexes

    def entropy_to_integer(self, users_list, entropies, entropy_num_classes):

        # Categorizes users into entropy group ranges
        series = pd.Series(entropies).describe()
        entropies_sorted = sorted(entropies)
        print(series)
        category_list = []
        categories = {}
        for i in range(1, entropy_num_classes):
            print("i: ", i, "in: ", int((len(entropies_sorted) - 1) * (i / entropy_num_classes)), "e: ", entropies_sorted[int((len(entropies_sorted) - 1) * (i / entropy_num_classes))])
            categories[i] = entropies_sorted[int((len(entropies_sorted) - 1) * (i / entropy_num_classes))]
        categories[entropy_num_classes] = 1
        # if entropy_num_classes == 4:
        #     categories = {0: series.loc['25%'], 1: series.loc['50%'], 2:series.loc['75%'], 3:series.loc['max']}
        # elif entropy_num_classes == 3:
        #     categories = {0: series.loc['25%'], 1: series.loc['50%'], 2: series.loc['max']}
        # elif entropy_num_classes == 2:
        #     categories = {0: series.loc['25%'], 1: series.loc['max']}
        users_list_updated = []
        for i in range(len(users_list)):
            entropy = entropies[i]
            user_list = users_list[i]
            user_list_updated = []
            for e in user_list:
                for category in categories:
                    if entropy <= categories[category]:
                        user_category = category
                        break
                break

            for e in user_list:
                aux = []
                for value in e:
                    aux.append(value)
                aux.append(user_category)
                aux = tuple(aux)
                user_list_updated.append(aux)

            users_list_updated.append(user_list_updated)

        #print("resul: ", pd.Series(category_list).describe())
        return users_list_updated

    def run_tests(self, n_tests, model_name, outputTime, epochs, time_num_classes, filename, n_location_outputs, optimizer,
                  class_weight, X_train, y_train, X_test, y_test):
        if not outputTime:
            if n_location_outputs == 2:
                return  self.run_tests_two_locations_output(n_tests=n_tests, model_name=model_name, epochs=epochs,
                                                            optimizer=optimizer, X_train=X_train, y_train=y_train,
                                                            X_test=X_test, y_test=y_test, filename=filename)
            elif n_location_outputs == 1:
                return self.run_tests_one_location_output(n_tests=n_tests, model_name=model_name, epochs=epochs,
                                                          optimizer=optimizer, class_weight=class_weight, X_train=X_train,
                                                          y_train=y_train, X_test=X_test, y_test=y_test, filename=filename)
        else:
            return self.run_test_location_time(n_tests=n_tests, model_name=model_name, epochs=epochs,  optimizer=optimizer,
                                               X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test,
                                               time_num_classes=time_num_classes, filename=filename)

    def run_test_location_time(self, n_tests, model_name, epochs, optimizer, X_train,
                  y_train, X_test, y_test, time_num_classes, filename):

        location_report_1 = {"Casa": {"precision": 0, "recall": 0, "f1-score": 0},
                             "Outro": {"precision": 0, "recall": 0, "f1-score": 0},
                             "Deslocamento": {"precision": 0, "recall": 0, "f1-score": 0}, "accuracy": 0}

        time_report = {str(i): {"precision": 0, "recall": 0, "f1-score": 0} for i in range(24)}
        time_report['accuracy'] = 0

        for i in range(n_tests):
            model = NextLocationPredictDomain().model(model_name)

            losses = {
                "location_output": "categorical_crossentropy",
                "time_output": "categorical_crossentropy",
            }
            lossWeights = {"location_output": 1.0, "time_output": 1.0}
            INIT_LR = 1e-3
            # opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
            model.compile(optimizer=optimizer, loss=["categorical_crossentropy", "categorical_crossentropy"],
                          loss_weights=[1, 1], metrics=['accuracy'])

            # % % time
            class_weight = {0: 0.4,
                            1: 0.3,
                            2: 0.3}

            # , class_weight=class_weight
            hi = model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=100, epochs=epochs)

            h = pd.DataFrame(hi.history)
            print("summary: ", model.summary())
            # print("history: ", h)

            y_predict_location, y_predict_time = model.predict(X_test, batch_size=10)

            scores = model.evaluate(X_test, y_test, verbose=0)
            # location_acc = scores
            # print(scores)

            # To transform one_hot_encoding to list of integers, representing the locations
            print("------------- Location ------------")
            y_predict_location = one_hot_decoding(y_predict_location)
            y_test_location = one_hot_decoding(y_test[0])
            # print("Original: ", y_test_location)
            # print("previu: ", y_predict_location)
            report = skm.classification_report(y_test_location, y_predict_location,
                                               target_names=['Casa', 'Outro', 'Deslocamento'], output_dict=True)
            location_report_1 = self.add_location_report(location_report_1, report)

            # To transform one_hot_encoding to list of integers, representing the locations
            print("------------ Time ------------")
            y_predict_time = one_hot_decoding(y_predict_time)
            y_test_time = one_hot_decoding(y_test[1])
            # print("Original: ", y_test)
            # print("previu: ", y_predict_time)
            report = skm.classification_report(y_test_time, y_predict_time, target_names=[str(i) for i in range(
                time_num_classes)], output_dict=True)
            time_report = self.add_time_report(time_report, report)

        # Calculate average for each metric
        location_report_1 = self.calculate_resultant_report(location_report_1, n_tests)

        time_report = self.calculate_resultant_report(time_report, n_tests)

        print("Relatorio final: " + model_name)
        print("Dataset: ", filename)
        print("Testes: ", n_tests, "Epocas: ", epochs)
        print("Location")
        print(location_report_1)

        print("Time")
        print(time_report)

        return location_report_1, time_report, h

    def run_tests_two_locations_output(self, n_tests: int, model_name: str, epochs, optimizer, X_train,
                                       y_train, X_test, y_test, filename):
        location_report_1 = {"Casa": {"precision": 0, "recall": 0, "f1-score": 0},
                             "Outro": {"precision": 0, "recall": 0, "f1-score": 0},
                             "Deslocamento": {"precision": 0, "recall": 0, "f1-score": 0}, "accuracy": 0}

        location_report_2 = {"Casa": {"precision": 0, "recall": 0, "f1-score": 0},
                             "Outro": {"precision": 0, "recall": 0, "f1-score": 0},
                             "Deslocamento": {"precision": 0, "recall": 0, "f1-score": 0}, "accuracy": 0}

        for i in range(n_tests):
            model = NextLocationPredictDomain().model(model_name)

            losses = {
                "location_output": "categorical_crossentropy",
                "time_output": "categorical_crossentropy",
            }
            lossWeights = {"location_output": 1.0, "time_output": 1.0}
            INIT_LR = 1e-3
            # opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
            model.compile(optimizer=optimizer, loss=["categorical_crossentropy", "categorical_crossentropy"],
                          loss_weights=[1, 1], metrics=['accuracy'])

            # % % time
            class_weight = {0: 0.4,
                            1: 0.3,
                            2: 0.3}

            # , class_weight=class_weight
            hi = model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=100, epochs=epochs)

            h = pd.DataFrame(hi.history)
            print("summary: ", model.summary())
            # print("history: ", h)

            y_predict_location, y_predict_time = model.predict(X_test, batch_size=10)

            scores = model.evaluate(X_test, y_test, verbose=0)
            # location_acc = scores
            # print(scores)

            # To transform one_hot_encoding to list of integers, representing the locations
            print("------------- Location ------------")
            y_predict_location = one_hot_decoding(y_predict_location)
            y_test_location = one_hot_decoding(y_test[0])
            # print("Original: ", y_test_location)
            # print("previu: ", y_predict_location)
            report = skm.classification_report(y_test_location, y_predict_location,
                                               target_names=['Casa', 'Outro', 'Deslocamento'], output_dict=True)
            location_report_1 = self.add_location_report(location_report_1, report)

            print("------------- Location ------------")
            y_predict_location = one_hot_decoding(y_predict_location)
            y_test_location = one_hot_decoding(y_test[1])
            # print("Original: ", y_test_location)
            # print("previu: ", y_predict_location)
            report = skm.classification_report(y_test_location, y_predict_location,
                                               target_names=['Casa', 'Outro', 'Deslocamento'], output_dict=True)
            location_report_2 = self.add_location_report(location_report_2, report)

        # Calculate average for each metric
        location_report_1 = self.calculate_resultant_report(location_report_1, n_tests)

        location_report_2 = self.calculate_resultant_report(location_report_2, n_tests)

        print("Relatorio final: " + model_name)
        print("Dataset: ", filename)
        print("Testes: ", n_tests, "Epocas: ", epochs)
        print("Location")
        print(location_report_1)
        print("Location 2")
        print(location_report_2)

        return location_report_1, location_report_2, h

    def run_tests_one_location_output(self, n_tests: int, model_name: str, epochs, optimizer, class_weight, X_train,
                                       y_train, X_test, y_test, filename):
        location_report_1 = {"Casa": {"precision": [], "recall": [], "f1-score": []},
                             "Outro": {"precision": [], "recall": [], "f1-score": []},
                             "Deslocamento": {"precision": [], "recall": [], "f1-score": []},
                             "accuracy": []}

        location_report_plot = {"precisao_casa": [],
                                "precisao_outro": [],
                                "precisao_deslocamento": [],
                                "revocacao_casa": [],
                                "revocacao_outro": [],
                                "revocacao_deslocamento": [],
                                "fscore_casa": [],
                                "fscore_outro": [],
                                "fscore_deslocamento": [],
                                "accurary": [],
                                "location": []}

        # print("y train: ", y_train.shape)
        # print(" y_test: ", y_test.shape)
        for i in range(n_tests):
            model = NextLocationPredictDomain().model(model_name)

            losses = {
                "location_output": "categorical_crossentropy",
                "time_output": "categorical_crossentropy",
            }
            lossWeights = {"location_output": 1.0, "time_output": 1.0}
            INIT_LR = 1e-3
            # opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
            model.compile(optimizer=optimizer, loss=["categorical_crossentropy"],
                          loss_weights=[1], metrics=['accuracy'])

            # % % time
            # class_weight = {0: 1,
            #                 1: 2,
            #                 2: 1}

            # , class_weight=class_weight
            hi = model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=100, epochs=epochs, class_weight=class_weight)

            h = pd.DataFrame(hi.history)
            print("summary: ", model.summary())
            # print("history: ", h)

            y_predict_location= model.predict(X_test, batch_size=100)

            scores = model.evaluate(X_test, y_test, verbose=0)
            # location_acc = scores
            # print(scores)

            # To transform one_hot_encoding to list of integers, representing the locations
            print("------------- Location ------------")
            y_predict_location = one_hot_decoding(y_predict_location)
            y_test_location = one_hot_decoding(y_test[0])
            # print("Original: ", y_test_location)
            # print("previu: ", y_predict_location)
            report = skm.classification_report(y_test_location, y_predict_location,
                                               target_names=['Casa', 'Outro', 'Deslocamento'], output_dict=True)
            location_report_1 = self.add_location_report(location_report_1, report)

        location_report = copy.deepcopy(location_report_1)

        print("lista: ", location_report_1)
        for l_key in location_report_1:
            if l_key == 'accuracy' or l_key == 'tests' or l_key == 'epochs':
                location_report_1[l_key] = str(location_report_1[l_key])
                continue
            for v_key in location_report_1[l_key]:
                location_report_1[l_key][v_key] = str(location_report_1[l_key][v_key])

        location_report_1['tests'] = n_tests
        location_report_1['epochs'] = epochs

        # Calculate average for each metric
        location_report = self.calculate_resultant_report(location_report, n_tests, epochs)
        plot_report = self.report_to_plot_report(pd.DataFrame(location_report_1))

        print("Relatorio final: " + model_name)
        print("Dataset: ", filename)
        print("Optimizer: ", optimizer)
        print("Testes: ", n_tests, "Epocas: ", epochs)
        print("Location")
        print(location_report)
        return location_report_1, None, h, plot_report


    def run_tests_one_location_output_k_fold(self, users_list, users_train_index, users_test_index, n_tests: int,
                                             model_name: str, epochs, optimizer, class_weight, filename, step_size):

        location_report_1 = {"Casa": {"precision": [], "recall": [], "f1-score": []},
                             "Outro": {"precision": [], "recall": [], "f1-score": []},
                             "Deslocamento": {"precision": [], "recall": [], "f1-score": []},
                             "accuracy": []}

        location_report_plot = {"precisao_casa": [],
                                "precisao_outro": [],
                                "precisao_deslocamento": [],
                                "revocacao_casa": [],
                                "revocacao_outro": [],
                                "revocacao_deslocamento": [],
                                "fscore_casa": [],
                                "fscore_outro": [],
                                "fscore_deslocamento": [],
                                "accurary": [],
                                "location": []}

        # print("y train: ", y_train.shape)
        # print(" y_test: ", y_test.shape)
        for i in range(n_tests):

            X_train, X_test, y_train, y_test = self.extract_train_test_from_indexes_k_fold(users_list=users_list,
                                                                                           users_train_indexes=users_train_index[i],
                                                                                           users_test_indexes=users_test_index[i],
                                                                                           step_size=step_size)
            model = NextLocationPredictDomain().model(model_name)

            losses = {
                "location_output": "categorical_crossentropy",
                "time_output": "categorical_crossentropy",
            }
            lossWeights = {"location_output": 1.0, "time_output": 1.0}
            INIT_LR = 1e-3
            # opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
            model.compile(optimizer=optimizer, loss=["categorical_crossentropy"],
                          loss_weights=[1], metrics=['accuracy'])

            # % % time
            # class_weight = {0: 1,
            #                 1: 2,
            #                 2: 1}

            # , class_weight=class_weight
            hi = model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=100, epochs=epochs, class_weight=class_weight)

            h = pd.DataFrame(hi.history)
            print("summary: ", model.summary())
            # print("history: ", h)

            y_predict_location= model.predict(X_test, batch_size=100)

            scores = model.evaluate(X_test, y_test, verbose=0)
            # location_acc = scores
            # print(scores)

            # To transform one_hot_encoding to list of integers, representing the locations
            print("------------- Location ------------")
            y_predict_location = one_hot_decoding(y_predict_location)
            y_test_location = one_hot_decoding(y_test[0])
            # print("Original: ", y_test_location)
            # print("previu: ", y_predict_location)
            report = skm.classification_report(y_test_location, y_predict_location,
                                               target_names=['Casa', 'Outro', 'Deslocamento'], output_dict=True)
            location_report_1 = self.add_location_report(location_report_1, report)

        location_report = copy.deepcopy(location_report_1)

        print("lista: ", location_report_1)
        for l_key in location_report_1:
            if l_key == 'accuracy' or l_key == 'tests' or l_key == 'epochs':
                location_report_1[l_key] = str(location_report_1[l_key])
                continue
            for v_key in location_report_1[l_key]:
                location_report_1[l_key][v_key] = str(location_report_1[l_key][v_key])

        location_report_1['tests'] = n_tests
        location_report_1['epochs'] = epochs

        # Calculate average for each metric
        location_report = self.calculate_resultant_report(location_report, n_tests, epochs)
        plot_report = self.report_to_plot_report(pd.DataFrame(location_report_1))

        print("Relatorio final: " + model_name)
        print("Dataset: ", filename)
        print("Optimizer: ", optimizer)
        print("Testes: ", n_tests, "Epocas: ", epochs)
        print("Location")
        print(location_report)
        return location_report_1, None, h, plot_report

    def extract_train_test_from_indexes_k_fold(self, users_list, users_train_indexes, users_test_indexes, step_size, location_num_classes=3, time_num_classes=24):

        X_train_concat = []
        X_test_concat = []
        y_train_concat = []
        y_test_concat = []
        for user_sequence in users_list:
            size = len(user_sequence)
            train_size = int(size * 0.7)
            test_size = size - train_size
            train = user_sequence[:train_size]
            test = user_sequence[train_size::]
            X_train, y_train = sequence_to_x_y(train, step_size)
            X_test, y_test = sequence_to_x_y(test, step_size)

            X_train_concat = X_train_concat + X_train
            X_test_concat = X_test_concat + X_test
            y_train_concat = y_train_concat + y_train
            y_test_concat = y_test_concat + y_test

        X_train = X_train_concat
        X_test = X_test_concat
        y_train = y_train_concat
        y_test = y_test_concat

        y_train_hours = return_hour_from_sequence_y(y_train)
        y_test_hours = return_hour_from_sequence_y(y_test)

        # Remove hours. Currently training without events hour
        # X_train = remove_hour_from_sequence_x(X_train)
        # X_test = remove_hour_from_sequence_x(X_test)
        y_train = remove_hour_from_sequence_y(y_train)
        y_test = remove_hour_from_sequence_y(y_test)

        # Sequence tuples to ndarray. Use it if the remove hour function was not called
        # X_train = sequence_tuples_to_ndarray_x(X_train)
        # X_test = sequence_tuples_to_ndarray_x(X_test)
        # y_train = sequence_tuples_to_ndarray_y(y_train)
        # y_test = sequence_tuples_to_ndarray_y(y_test)

        # Sequence tuples to [spatial[,step_size], temporal[,step_size]] ndarray. Use with embedding layer.
        X_train = sequence_tuples_to_spatial_temporal_and_feature3_ndarrays(X_train, id=True)
        X_test = sequence_tuples_to_spatial_temporal_and_feature3_ndarrays(X_test, id=True)

        # X_train = np.asarray(X_train)
        # X_train = X_train.reshape(X_train.shape + (1,)) # perform it if the step doesn't have hour
        y_train = np.asarray(y_train)
        # X_test = np.asarray(X_test)
        # X_test = X_test.reshape(X_test.shape + (1,))
        y_test = np.asarray(y_test)

        # Convert integers to one-hot-encoding. It is important to convert the y (labels) to that.

        # X_train = np_utils.to_categorical(X_train)
        # X_test = np_utils.to_categorical(X_test)

        y_train_2 = np_utils.to_categorical(y_train, num_classes=location_num_classes)
        y_train = np_utils.to_categorical(y_train, num_classes=location_num_classes)
        y_train_hours = np_utils.to_categorical(y_train_hours, num_classes=time_num_classes)
        y_test_2 = np_utils.to_categorical(y_test, num_classes=location_num_classes)
        y_test = np_utils.to_categorical(y_test, num_classes=location_num_classes)
        y_test_hours = np_utils.to_categorical(y_test_hours, num_classes=time_num_classes)

        # when using multiple output [location, hour]
        y_train = [y_train]
        y_test = [y_test]

        return X_train, X_test, y_train, y_test

    def add_location_report(self, location_report, report):
        for l_key in location_report:
            if l_key == 'accuracy':
                location_report[l_key].append(report[l_key])
                continue
            for v_key in location_report[l_key]:
                location_report[l_key][v_key].append(report[l_key][v_key])

        return location_report

    def add_time_report(self, time_report, report):
        for l_key in time_report:
            if l_key == 'accuracy':
                time_report[l_key] = time_report[l_key] + report[l_key]
                continue
            for v_key in time_report[l_key]:
                time_report[l_key][v_key] = time_report[l_key][v_key] + report[l_key][v_key]

        return time_report

    def calculate_resultant_report(self, report, n_tests, epochs):

        new_report = copy.deepcopy(report)
        for l_key in report:
            if l_key == 'accuracy':
                new_report[l_key] = sum(report[l_key]) / n_tests
                continue
            for v_key in report[l_key]:
                new_report[l_key][v_key] = sum(report[l_key][v_key]) / n_tests

        new_report['tests'] = n_tests
        new_report['epochs'] = epochs

        return new_report

    def report_to_plot_report(self, df):

        columns = []
        for column in df.columns:
            if column not in ["metric", "accuracy", "epochs", "tests"]:
                columns.append(column)
        names = []
        indexes = df.index.tolist()
        # for index in df.index.tolist():
        #     if index not in ["metric", "acuracy", "epochs", "tests"]:
        #         indexes.append(index)

        values = []

        new_df = {}

        for index in indexes:
            row = df.loc[index]
            values = []
            for column in columns:
                value = ast.literal_eval(row[column])
                size = len(value)
                name = [column]*size
                values = values + value

            new_df[index] = values

        for column in columns:
            names = names + [column]*size

        new_df['location'] = names

        return pd.DataFrame(new_df)

    def day_type(self, datetime):

        if datetime.weekday() < 5:
            return 0
        else:
            return 1

