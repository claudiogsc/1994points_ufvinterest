#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import statistics as sts
import datetime as dt
from models.identification_classification_models import events
import statistics as st
import math
import copy


class Poi:

    def __init__(self, centroid, poi_class, coordinates, times):
        self._centroid = centroid # longitude, latitude
        self._coordinates = coordinates
        self._total_points = len(coordinates)
        # self.__retangulo = self.set__Retangulo(coordenadas) #stay region
        self._times = events.Events(copy.deepcopy(times))
        self._total_hours = 0
        self._total_week_days = 0
        # Só usado quando existe validação da classificação
        self._poi_real_class = poi_class
        self._poi_classified_class = "Outro"
        self._total_home_events = 0
        self._total_work_events = 0
        self._different_days = 0
        self._period = 0
        self._week_events = 0
        self._weekend_events = 0

        self._total_first_last_events = 0
        self._first_events = []
        self._last_events = []

        self._n_events = len(coordinates)
        self._n_events_week = 0
        self._n_events_weekend = 0

        self._home_time = 0
        self._work_time = 0

        self._entropy_home_correct = -1
        self._entropy_home_wrong = -1
        self._entropy_work_correct = -1
        self._entropy_work_wrong = -1

        self._probability_home_events = 0
        self._probability_non_home_events = 0
        self._probability_work_events = 0
        self._probability_non_work_events = 0

        self._mean_week_event = 0

        self._different_hours = 0

        self._week_n_events_day_hours = [0] * 24
        self._weekend_n_events_day_hours = [0] * 24
        self._n_events_day_hours = [0] * 24

        self._has_inactive_interval = False
        self._inactive_hours = []

        self._days_n_events_hours = {0: [0] * 24,
                                  1:[0] * 24,
                                  2:[0] * 24,
                                  3:[0] * 24,
                                  4:[0] * 24,
                                  5:[0] * 24,
                                  6:[0] * 24}

        self.calculate_period()
        self.calculate_different_days_and_hours()
        self.calculate_n_events_day_hours()
        self.calculate_n_events_day_hours_all()

    def number_of_events_on_week_weekend(self):

        week = 0
        weekend = 0

        for e in self._week_n_events_day_hours:
            if e != 0:
                week = week + 1

        for e in self._weekend_n_events_day_hours:
            if e != 0:
                weekend = weekend + 1

        return week, weekend, self._week_n_events_day_hours, self._weekend_n_events_day_hours

    @property
    def mean_week_event(self):
        return self._mean_week_event

    @property
    def coordinates(self):
        return self._coordinates

    @property
    def times(self):
        return self._times

    @property
    def total_hours(self):
        return self._total_hours

    @property
    def total_week_days(self):
        return self._total_week_days

    @property
    def different_days(self):
        return self._different_days

    @property
    def total_points(self):
        return self._total_points

    @property
    def horarios_diferentes(self):
        return self._horarios_diferentes

    def __str__(self):
        return "centroid: " + str(self._centroid)

    @property
    def centroid(self):
        return self._centroid

    @property
    def centroid(self):
        return self._centroid

    @property
    def latitude(self):
        return self._centroid[1]

    @property
    def longitude(self):
        return self._centroid[0]

    @property
    def poi_real_class(self):
        return self._poi_real_class

    @property
    def poi_classified_class(self):
        return self._poi_classified_class

    @poi_classified_class.setter
    def poi_classified_class(self, poi_classified_class):
        self._poi_classified_class = poi_classified_class

    @property
    def home_time(self):
        return self._home_time

    @property
    def work_time(self):
        return self._work_time

    @home_time.setter
    def home_time(self, time):
        self._home_time = time

    @work_time.setter
    def work_time(self, time):
        self._work_time = time

    @property
    def total_home_events(self):
        return self._total_home_events

    @property
    def total_work_events(self):
        return self._total_work_events

    @property
    def has_inactive_interval(self):
        self._has_inactive_interval

    @has_inactive_interval.setter
    def has_inactive_interval(self, has_inactive_interval):
        self._has_inactive_interval = has_inactive_interval

    @property
    def inactive_hours(self):
        self._inactive_hours

    @inactive_hours.setter
    def inactive_hours(self, inactive_hours):
        self._inactive_hours = inactive_hours

    @property
    def total_first_last_events(self):
        return self._total_first_last_events

    @property
    def week_events(self):
        return self._week_events

    @property
    def weekend_events(self):
        return self._weekend_events

    @property
    def different_hours(self):
        return self._different_hours

    @property
    def area(self):
        return self._area

    @property
    def first_events(self):
        return self._first_events

    @property
    def probability_home_events(self):
        return self._probability_home_events

    @property
    def probability_non_home_events(self):
        return self._probability_non_home_events

    @property
    def probability_work_events(self):
        return self._probability_work_events

    @property
    def probability_non_work_events(self):
        return self._probability_non_work_events

    @property
    def last_events(self):
        return self._last_events

    @property
    def n_events(self):
        return self._n_events

    @property
    def week_n_events_day_hours(self):
        return self._week_n_events_day_hours

    @property
    def weekend_n_events_day_hours(self):
        return self._weekend_n_events_day_hours

    @property
    def days_n_events_hours(self):
        return self._days_n_events_hours

    @property
    def n_events_day(self):
        return self._n_events_day

    @property
    def n_events_night(self):
        return self._n_events_night

    @property
    def n_events_week(self):
        return self._n_events_week

    @property
    def n_events_weekend(self):
        return self._n_events_weekend

    @property
    def n_events_day_week(self):
        return self._n_events_day_week

    @property
    def n_events_day_hours(self):
        return self._n_events_day_hours

    @property
    def n_events_night_week(self):
        return self._n_events_night_week

    @property
    def n_events_day_weekend(self):
        return self._n_events_day_weekend

    @property
    def n_events_night_weekend(self):
        return self._n_events_night_weekend

    @n_events.setter
    def n_events(self, n):
        self._n_events = n

    def set_inactive_hours(self, hours):
        self._inactive_hours = hours

    def set_has_inactive_interval(self):
        self._has_inactive_interval = True

    def mean_first_events(self):
        return sts.mean(self._first_events)

    def mean_last_events(self):
        return sts.mean(self._last_events)

    def median_first_events(self):
        return sts.median(self._first_events)

    def median_last_events(self):
        return sts.median(self._last_events)

    def add_total_first_last_events(self):
        self._total_first_last_events = self._total_first_last_events + 1

    def add_first_events(self, datetime):
        self._first_events.append(datetime)

    def add_last_events(self, datetime):
        self._last_events.append(datetime)

    def __eq__(self, other):
        if other != None:
            return self._centroid == other.centroid
        else:
            False

    def add_home_events(self):
        self._total_home_events = self._total_home_events + 1

    def add_work_events(self):
        self._total_work_events = self._total_work_events + 1

    def add_n_events(self):
        self._n_events = self._n_events + 1

    def add_n_events_day(self):
        self._n_events_day = self._n_events_day + 1

    def add_n_events_night(self):
        self._n_events_night = self._n_events_night + 1

    def add_n_events_week(self):
        self._n_events_week = self._n_events_week + 1

    def add_n_events_weekend(self):
        self._n_events_weekend = self._n_events_weekend + 1

    def add_n_events_day_week(self):
        self._n_events_day_week = self._n_events_day_week + 1

    def add_n_events_night_week(self):
        self._n_events_night_week = self._n_events_night_week + 1

    def add_n_events_day_weekend(self):
        self._n_events_day_weekend = self._n_events_day_weekend + 1

    def add_n_events_night_weekend(self):
        self._n_events_night_weekend = self._n_events_night_weekend + 1

    def calculate_n_events_proposta(self):
        times = self._times.times
        home_hour = self._home_time
        work_hour = self._work_time
        for i in range(len(times)):
            datatempo = times[i]
            if (home_hour['start'] > home_hour['end'] and (
                    datatempo.hour >= home_hour['start'] or datatempo.hour <= home_hour['end'])):
                self.add_home_events()
            else:
                if (datatempo.hour >= home_hour['start'] or datatempo.hour <= home_hour['end']):
                    self.add_home_events()
            if datatempo.weekday() >= 5:
                continue
            elif work_hour['start'] < work_hour['end'] and datatempo.hour >= work_hour['start'] and datatempo.hour <= \
                    work_hour['end']:
                self.add_work_events()
            else:
                if ((datatempo.hour >= work_hour['start'] or datatempo.hour <= work_hour['end'])):
                    self.add_work_events()

    def calculate_n_events(self):
        times = self._times.times
        home_hour = self._home_time
        work_hour = self._work_time
        for i in range(len(times)):
            datatempo = times[i]
            if datatempo.weekday() >= 5:
                continue
            if (home_hour['start'] > home_hour['end'] and (
                    datatempo.hour >= home_hour['start'] or datatempo.hour <= home_hour['end'])):
                self.add_home_events()
            else:
                if (datatempo.hour >= home_hour['start'] or datatempo.hour <= home_hour['end']):
                    self.add_home_events()
            if work_hour['start'] < work_hour['end'] and datatempo.hour >= work_hour['start'] and datatempo.hour <= \
                    work_hour['end']:
                self.add_work_events()
            else:
                if (work_hour['end'] < work_hour['start'] and (
                        datatempo.hour >= work_hour['start'] or datatempo.hour <= work_hour['end'])):
                    self.add_work_events()

    def calculate_n_events_day_hours(self):
        times = sorted(self._times.times)
        week_n_events_day_hours = [0] * 24
        weekend_n_events_day_hours = [0] * 24
        days_n_events_hours = {0: [0] * 24,
                                  1:[0] * 24,
                                  2:[0] * 24,
                                  3:[0] * 24,
                                  4:[0] * 24,
                                  5:[0] * 24,
                                  6:[0] * 24}
        for i in range(len(times)):
            datetime = times[i]
            hour = datetime.hour
            day_number = datetime.weekday()
            if datetime.weekday() < 5:
                week_n_events_day_hours[hour] = week_n_events_day_hours[hour] + 1
            else:
                weekend_n_events_day_hours[hour] = weekend_n_events_day_hours[hour] + 1
            days_n_events_hours[day_number][hour] = days_n_events_hours[day_number][hour] + 1

        self._week_n_events_day_hours = week_n_events_day_hours
        self._weekend_n_events_day_hours = weekend_n_events_day_hours
        self._days_n_events_hours = days_n_events_hours

    def calculate_n_events_day_hours_all(self):
        times = sorted(self._times.times)
        n_events_day_hours = [0] * 24
        for i in range(len(times)):
            datetime = times[i]
            hour = datetime.hour
            n_events_day_hours[hour] = n_events_day_hours[hour] + 1

        self._n_events_day_hours = n_events_day_hours

    def to_dataframe(self, total_user_home_events=None, total_user_work_events=None, total_user_events=None,
                     total_user_different_days=None, total_user_week_events=None, total_user_weekend_events=None):
        poi_real_class = self._poi_real_class
        if poi_real_class == "Lazer":
            poi_real_class = "Outro"

        if total_user_home_events != None and total_user_work_events != None and total_user_events != None and total_user_different_days != None and total_user_week_events != None and total_user_weekend_events != None:
            if total_user_home_events == 0:
                home_percentage = 0
                print("casa 0")
            else:
                home_percentage = self._total_home_events / total_user_home_events
            if total_user_work_events == 0:
                work_percentage = 0
                print("trabalho 0")
            else:
                work_percentage = self._total_work_events / total_user_work_events
            if total_user_events == 0:
                total_percentage = 0
                print("total 0")
                home_percentage_total_events = 0
                work_percentage_total_events = 0
                different_days_percentage = 0
                total_user_week_events = 0
                total_user_weekend_events = 0
            else:
                total_percentage = self._total_points / total_user_events
                home_percentage_total_events = self._total_home_events / total_user_events
                work_percentage_total_events = self._total_work_events / total_user_events
                different_days_percentage = self._different_days / total_user_different_days
            if total_user_week_events == 0:
                week_events = 0
            else:
                week_events = self._week_events / total_user_week_events
            if total_user_weekend_events == 0:
                weekend_events = 0
            else:
                weekend_events = self.weekend_events / total_user_weekend_events

            return pd.DataFrame({'home_percentage_total_events': [home_percentage_total_events],
                                 'work_percentage_total_events': [work_percentage_total_events],
                                 'total_points': [total_percentage], 'home_time_events': [home_percentage],
                                 'work_time_events': [work_percentage], 'different_days': [different_days_percentage],
                                 'period': [self._period], 'week_events': [week_events],
                                 'weekend_events': [weekend_events], 'classe': [poi_real_class]})

        return pd.DataFrame({'total_points': [self._total_points], 'home_time_events': [self._total_home_events],
                             'work_time_events': [self._total_work_events],
                             'different_days': [self._different_days_percentage], 'period': [self._period],
                             'classe': [poi_real_class]})

    def calculate_probabilities_home_work_events(self):

        if self._total_points > 0:
            self._probability_home_events = self._total_home_events
            self._probability_non_home_events = (self._total_points - self._total_home_events)
            self._probability_work_events = self._total_work_events
            self._probability_non_work_events = (self._total_points - self._total_work_events)

    def compare_poi_classes(self, classe):
        return self._poi_real_class == classe

    def probabilities(self):
        return [self._probability_home_events, self._probability_non_home_events]

    def mean_week_events(self):
        times = self._times.times
        start = times[0]
        end = times[-1]
        elapsed = end - start
        weeks = math.ceil(elapsed.days / 7)
        events_weeks = [0] * (weeks)
        j = 0
        date_before = None
        if weeks == 0:
            self._mean_week_events = 0
            return
        for i in range(len(times)):
            datatempo = times[i]

            if datatempo.date() <= (start + dt.timedelta(days=7 * (j + 1))).date():
                if date_before != None:
                    if datatempo.date() > date_before:
                        events_weeks[j] = events_weeks[j] + 1
                else:
                    events_weeks[j] = events_weeks[j] + 1
            else:
                events_weeks[j] = events_weeks[j] + 1
                j = j + 1
            date_before = datatempo.date()

        mean = st.mean(events_weeks)
        self._mean_week_event = mean

    def calculate_period(self):
        times = self._times.times

        self._period = (times[-1] - times[0]).days

    def calculate_different_days_and_hours(self):
        times = sorted(self._times.times)
        self._different_days = 1
        data_before = times[0].date()
        day_hours = [0] * 24
        for i in range(len(times)):
            data = times[i].date()
            if data > data_before:
                self._different_days = self._different_days + 1
            data_before = data
        times = sorted(self._times.times)
        for time in times:
            datetime = time
            if datetime.weekday() > 4:
                self._weekend_events = self._weekend_events + 1
            else:
                self._week_events = self._week_events + 1
            day_hours[datetime.hour] = 1

        for hour in day_hours:
            if hour > 0:
                self._different_hours = self._different_hours + 1
        # print("dias diferentes: ", self._different_days)