from models.identification_classification_models.poi import *
from models.identification_classification_models import user as user
import numpy as np
from sklearn.cluster.dbscan_ import DBSCAN
from configurations.identification_classification_config import N_AMOSTRAS, EPS, RESTRICTION

class Identification:

    def __init__(self):

        pass

    def centroid(self, vertexes):
        _x_list = [vertex[0] for vertex in vertexes]
        _y_list = [vertex[1] for vertex in vertexes]
        _len = len(vertexes)
        _x = sum(_x_list) / _len
        _y = sum(_y_list) / _len
        return (_x, _y)

    def clustering(self, coordenadas, idusuario, geo, datetimes):

        menor_tempo = datetimes[0]
        maior_tempo = datetimes[-1]
        periodo = maior_tempo - menor_tempo
        """if restricao:
            n_amostras=int(round(periodo.days*0.20,0))"""
        db = DBSCAN(eps=EPS, min_samples=N_AMOSTRAS, algorithm='ball_tree', metric='haversine').fit(np.radians(geo))

        pois_coordinates = {}
        labels = db.labels_
        tempos_listas = {}
        coordenadas_deslocamento = []
        tempos_deslocamento = []
        for i in range(len(list(set(labels)))):
            if i != -1:
                pois_coordinates[i] = []
                tempos_listas[i] = []
        for i in range(len(coordenadas)):
            if labels[i] != -1:
                pois_coordinates[labels[i]].append(coordenadas[i])
                tempos_listas[labels[i]].append(datetimes[i])
            else:
                coordenadas_deslocamento.append(coordenadas[i])
                tempos_deslocamento.append(datetimes[i])

        pois = []
        for i in range(len(pois_coordinates)):
            if len(pois_coordinates[i]) == 0:
                continue
            centroide = self.centroid(pois_coordinates[i])
            # f=frequencia_horario(labels, tempos.copy(), i)
            p = Poi(centroide, "", pois_coordinates[i], tempos_listas[i])
            if RESTRICTION:
                if p.different_days < int(round(0.15 * periodo.days, 0)):
                    continue
                # if p.different_hours < 7:
                #     continue
            pois.append(p)

        return pois, coordenadas_deslocamento, tempos_deslocamento

    def identify_points_of_interest(self, dados: dict):
        print("---- Identificando pontos de interesse ----")
        usuarios = []
        n_usuarios_to_process = 0
        for i in dados.keys():
            lo_la = []
            la_lo = []
            datetimes = dados[i]['datetime'].tolist()
            la = dados[i]['latitude'].tolist()
            lo = dados[i]['longitude'].tolist()
            for j in range(0, len(lo)):
                lo_la.append((lo[j], la[j]))
                la_lo.append((la[j], lo[j]))

            pois, coordenadas_deslocamento, tempos_deslocamento = self.clustering(lo_la, i, la_lo,
                                                                                  datetimes)
            if len(pois) > 0:
                n_usuarios_to_process = n_usuarios_to_process + 1
                # if n_usuarios_to_process > N_USUARIOS:
                #     break
                usuario = user.User(int(i), pois, coordenadas_deslocamento, tempos_deslocamento, la_lo, datetimes)
                usuarios.append(usuario)
        return usuarios