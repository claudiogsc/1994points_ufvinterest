import pandas as pd

class Rotina:

    def __init__(self):

        self.probabiities = None
        self.probabiities_list = None

    def set_probabilities(self, probabilities: list):

        self.probabiities = pd.DataFrame(data = probabilities, columns = [str(i) for i in range(24)], index = ['Home', 'Other', 'Displacement', 'Predicted_place'])

    def get_probabilities_list(self):

        return self.probabiities_list
