from keras.datasets import imdb
from keras.layers import GRU, LSTM, CuDNNGRU, CuDNNLSTM, Activation, Dense, Masking, Dropout, SimpleRNN, Input, Lambda, \
    Flatten, Reshape
from keras.layers.merge import add,concatenate
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
import pandas as pd
import numpy as np
#from matplotlib import pyplot
from keras.utils import np_utils
import sklearn.metrics as skm
from utils.next_location_predict_util import sequence_to_x_y, remove_hour_from_sequence_x, \
    remove_hour_from_sequence_y, sequence_tuples_to_ndarray_x, sequence_tuples_to_ndarray_y, one_hot_decoding, \
    hours_shift_x, sequence_tuples_to_spatial_and_temporal_ndarrays, return_hour_from_sequence_y, plot_history_metrics, \
    save_report
from keras.layers.embeddings import Embedding
import tensorflow as tf
import keras.backend as K
from keras.models import Model
from keras_multi_head import MultiHeadAttention
import tensorflow as tf

from configurations.next_location_predict_config import STEP_SIZE, \
    N_USUARIOS, RNNs, N_TESTES, VALIDATION_METRICS_NAMES, FIGURES_DIR, EPOCHS, \
    DATASET, TIME_OUTPUT, N_LOCATION_OUTPUT, OPTIMIZER, CLASS_WEIGHT, N_SPLITS
from models.next_location_predict_model.gru_enhanced import GRUenhaced
from domains.next_location_predict_domain import NextLocationPredictDomain

class NextLocationPredictJob:

    def __init__(self, solution_id):
        self.solution_id = solution_id
        self.nextlocationdomain = NextLocationPredictDomain()

    def start(self):

        if N_SPLITS == 0:
            X_train, X_test, y_train, y_test = self.nextlocationdomain.extract_train_test_dataset(
                self.solution_id,
                DATASET[self.solution_id]["file_name"],
                3,
                DATASET[self.solution_id]["time_num_classes"],
                DATASET[self.solution_id]["use_entropy"],
                DATASET[self.solution_id]["entropy_num_classes"],
                TIME_OUTPUT[self.solution_id],
                N_LOCATION_OUTPUT[self.solution_id],
                DATASET[self.solution_id]["features"],
                STEP_SIZE)

            report_1, report_2, h, plot_report = self.nextlocationdomain.run_tests(N_TESTES, RNNs[self.solution_id],
                                                                                   TIME_OUTPUT[self.solution_id],
                                                                                   EPOCHS,
                                                                                   DATASET[self.solution_id][
                                                                                       'time_num_classes'],
                                                                                   DATASET[self.solution_id][
                                                                                       'file_name'],
                                                                                   N_LOCATION_OUTPUT[self.solution_id],
                                                                                   OPTIMIZER[self.solution_id],
                                                                                   CLASS_WEIGHT[self.solution_id],
                                                                                   X_train,
                                                                                   y_train, X_test, y_test)

        else:
            users_list, users_train_indexes, users_test_indexes = self.nextlocationdomain.extract_train_test_indexes_k_fold(
                DATASET[self.solution_id]["file_name"],
                DATASET[self.solution_id]["use_entropy"],
                DATASET[self.solution_id]["entropy_num_classes"],
                N_SPLITS)

            report_1, report_2, h, plot_report = self.nextlocationdomain.run_tests_one_location_output_k_fold(users_list=users_list,
                                                                                                              users_train_index=users_train_indexes,
                                                                                                              users_test_index=users_test_indexes,
                                                                                                              model_name=RNNs[self.solution_id],
                                                                                                              n_tests=N_TESTES,
                                                                                                              epochs=EPOCHS,
                                                                                                              optimizer=OPTIMIZER[self.solution_id],
                                                                                                              class_weight=CLASS_WEIGHT[self.solution_id],
                                                                                                              filename=DATASET[self.solution_id]["file_name"],
                                                                                                              step_size=STEP_SIZE)

        plot_history_metrics(h, VALIDATION_METRICS_NAMES[self.solution_id], FIGURES_DIR[self.solution_id])
        cont = 1
        for report in [report_1, report_2]:
            if report != None:
                save_report("report_" + str(cont), N_TESTES, EPOCHS, report, FIGURES_DIR[self.solution_id])
                cont = cont + 1

        save_report("plot_report", N_TESTES, EPOCHS, plot_report, FIGURES_DIR[self.solution_id])